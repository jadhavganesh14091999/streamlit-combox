import { useState } from "react";
import Picker from "./components/Picker";
import { Client as Styletron } from "styletron-engine-atomic";
import { Provider as StyletronProvider } from "styletron-react";
import { LightTheme, BaseProvider } from "baseui";

const engine = new Styletron();

function App() {
  return (
    <StyletronProvider value={engine}>
      <BaseProvider theme={LightTheme}>
        <Picker />
      </BaseProvider>
    </StyletronProvider>
  );
}

export default App;
