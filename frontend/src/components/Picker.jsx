import React, { useState, useEffect } from "react";
import { Streamlit, withStreamlitConnection } from "streamlit-component-lib";
import { Combobox } from "baseui/combobox";

import FormControl from "@mui/material/FormControl";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import InputLabel from "@mui/material/InputLabel";

const Picker = (props) => {
  const [value, setValue] = useState("");

  const options = props.args.options;

  // useEffect(() => {
  //   Streamlit.setFrameHeight(150);
  // }, []);
  Streamlit.setFrameHeight(300);

  const valueHandler = (nextValue) => {
    setValue(nextValue);
    Streamlit.setComponentValue(nextValue);
  };
  return (
    <>
      <FormControl fullWidth>
        <InputLabel>Explore</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={value}
          label="Age"
          onChange={(event) => valueHandler(event.target.value)}>
          {options.map((option) => (
            <MenuItem key={option} value={option}>
              {option}
            </MenuItem>
          ))}
        </Select>
      </FormControl>

      {/* <div>
        <Combobox
          value={value}
          onChange={(nextValue) => setValue(nextValue)}
          options={[
            { label: "AliceBlue", id: "#F0F8FF" },
            { label: "AntiqueWhite", id: "#FAEBD7" },
            { label: "Aqua", id: "#00FFFF" },
            { label: "Aquamarine", id: "#7FFFD4" },
            { label: "Azure", id: "#F0FFFF" },
            { label: "Beige", id: "#F5F5DC" },
          ]}
          mapOptionToString={(option) => option.label}
        />
      </div> */}
    </>
  );
};

export default withStreamlitConnection(Picker);
